import 'package:flutter/material.dart';
import 'package:plantimizer/utils/const/app_colors.dart';

class CustomTextWithIconField extends StatelessWidget {
  String hintText;
  CustomTextWithIconField({
    super.key,
    this.hintText = "",
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 311,
        height: 50,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(
            10,
          ),
          border: Border.all(
            color: AppColor.primaryColor,
            width: 0.0,
          ),
        ),
        child: TextFormField(
          decoration: InputDecoration(
            suffixIcon: const Icon(
              Icons.remove_red_eye_outlined,
              color: AppColor.textFieldContentColor,
            ),
            contentPadding: const EdgeInsets.only(
              left: 20,
              top: 17,
            ),
            hintText: hintText,
            hintStyle: const TextStyle(
              color: AppColor.textFieldContentColor,
              fontSize: 14,
              fontWeight: FontWeight.w400,
              fontStyle: FontStyle.normal,
            ),
            focusedBorder: InputBorder.none,
            errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(
                10,
              ),
              borderSide: const BorderSide(
                color: AppColor.primaryColor,
                width: 0.0,
              ),
            ),
            border: InputBorder.none,
          ),
        ),
      ),
    );
  }
}

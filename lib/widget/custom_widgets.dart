import 'package:flutter/material.dart';

///Montserrat Text
Widget appMontserratText({
  String text = "",
  double size = 10,
  Color? color,
  FontWeight fontWeight = FontWeight.normal,
  int maxLines = 3,
  TextAlign textAlign = TextAlign.left,
}) {
  return Text(
    text,
    maxLines: maxLines,
    textAlign: textAlign,
    style: TextStyle(
      color: color,
      fontSize: size,
      fontFamily: "Montserrat",
      fontWeight: fontWeight,
      fontStyle: FontStyle.normal,
    ),
  );
}

///Poppins Text
Widget appPoppinsText({
  String text = "",
  double size = 10,
  Color? color,
  FontWeight fontWeight = FontWeight.normal,
  int maxLines = 3,
  TextAlign textAlign = TextAlign.left,
}) {
  return Text(
    text,
    maxLines: maxLines,
    textAlign: textAlign,
    style: TextStyle(
      color: color,
      fontSize: size,
      fontFamily: "Poppins",
      fontWeight: fontWeight,
      fontStyle: FontStyle.normal,
    ),
  );
}

///Vertical Space
Widget verticalSpace(
  double h,
) {
  return SizedBox(
    height: h,
  );
}

///Horizontal Space
Widget horizontalSpace(
  double w,
) {
  return SizedBox(
    width: w,
  );
}

double screenWidth(BuildContext context) {
  return MediaQuery.of(context).size.width;
}

double screenHeight(BuildContext context) {
  return MediaQuery.of(context).size.height;
}

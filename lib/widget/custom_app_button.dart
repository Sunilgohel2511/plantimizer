import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:plantimizer/utils/const/app_colors.dart';

class CustomAppButton extends StatelessWidget {
  const CustomAppButton({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: 300,
      height: 50,
      decoration: BoxDecoration(
        color: AppColor.primaryColor,
        borderRadius: BorderRadius.circular(
          10,
        ),
      ),
      child: const Text(
        "Login",
        style: TextStyle(
          color: AppColor.white,
          fontWeight: FontWeight.w700,
          fontSize: 16,
          fontStyle: FontStyle.normal,
        ),
      ),
    );
  }
}

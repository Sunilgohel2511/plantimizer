import 'package:flutter/material.dart';

class AppColor {
  AppColor._();

  static const Color primaryColor = Color(0xFF62A770);
  static const Color blackColor = Color(0xFF000000);
  static const Color textFieldContentColor = Color(0XFF343434);
  static const Color white = Colors.white;
}

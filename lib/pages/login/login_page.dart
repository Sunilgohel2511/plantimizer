import 'package:flutter/material.dart';
import 'package:plantimizer/utils/const/app_colors.dart';
import 'package:plantimizer/widget/custom_app_button.dart';
import 'package:plantimizer/widget/custom_text_field.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Scaffold(
        extendBodyBehindAppBar: true,
        // bottomNavigationBar: loginWidget(),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 40,
              ),
              Center(
                child: Image.asset(
                  'assets/images/app_logo.png',
                  width: 56,
                  height: 56,
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              const Center(
                child: Text(
                  "Login",
                  style: TextStyle(
                    color: AppColor.primaryColor,
                    fontWeight: FontWeight.w700,
                    fontSize: 32,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              const Center(
                child: Text(
                  "Sign in to your account",
                  style: TextStyle(
                    color: AppColor.blackColor,
                    fontWeight: FontWeight.w400,
                    fontSize: 16,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ),
              const SizedBox(
                height: 32,
              ),
              CustomTextField(
                hintText: "Enter your e-mail adress",
              ),
              const SizedBox(
                height: 16,
              ),
              CustomTextField(hintText: "Enter your password"),
              const SizedBox(
                height: 16,
              ),
              const Padding(
                padding: EdgeInsets.only(left: 32.0, right: 32.0),
                child: Text(
                  "Forgot password?",
                  style: TextStyle(
                    color: AppColor.blackColor,
                    fontWeight: FontWeight.w400,
                    fontSize: 12,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ),
              loginWidget()
            ],
          ),
        ),
      ),
    );
  }

  Widget loginWidget() {
    return Column(
      // mainAxisAlignment: MainAxisAlignment.end,
      // crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        const Center(
          child: CustomAppButton(),
        ),
        const SizedBox(
          height: 16,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: const [
            Text(
              "I don’t have an account. ",
              style: TextStyle(
                color: AppColor.blackColor,
                fontWeight: FontWeight.w400,
                fontSize: 14,
                fontStyle: FontStyle.normal,
              ),
            ),
            Text(
              "Register",
              style: TextStyle(
                color: AppColor.blackColor,
                fontWeight: FontWeight.w700,
                fontSize: 14,
                fontStyle: FontStyle.normal,
              ),
            ),
          ],
        ),
      ],
    );
  }
}
